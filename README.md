# Festivals 2018

### Descripción

Bienvenido a la documentación de la aplicación nativa  de iOS de los festivales mas importantes de ocesa, explora la aplicación nativa que COCOLAB ha creado y continúa construyendo, implementando y mejorando. Este repositorio contiene documentación técnica de la plataforma festivales.

### Empezando

##### Pre-requisitos

- [Git](https://git-scm.com/)
- [Xcode](https://developer.apple.com/xcode/)
- [CocoaPods](https://cocoapods.org/)

##### Instalación

- Abrir terminal
- Ejecutar `git clone --recursive` para clonar el repositorio con el       submodulo
- Ejecutar `cd...` en el directorio recien clonado 
- Ejecutar `pod install` para instalar las dependencias
- Ejecutar `cd/.../Engine` para  cambiar al directorio del submodulo
- Ejecutar `git checkout master` para cambiar a la rama maestra
- Abrir el archivo `Festivals.xcworkspace`  y en el menu `Xcode/File/Workspace Settings...`
- Elegir en la opción Build System: Legacy Build System.
- Ejecutar boton play y elegir algún dispositivo de Simulación o Dispositivo 

### Creando un nuevo festival

1. Crea un fork de este repositorio con el nombre del evento.

2. Cambia los identificadores principales listados a continuación en el archivo Info.plist:

 - ID evento (CMS)
 - Onesignal APP ID
 - Bundle IOS
 - Bundle Display Name
 - Bundle Name and Build
 - Version
 - Facebook App ID (incluyendo URL schemes)
 - Facebook Display Name
 - Krux ID
 
3. Cambia los colores en la clase `setViewColor.m`  de acuerdo al diseño UI.

4. [Crear certificado para las notificaciones push](https://documentation.onesignal.com/docs/generate-an-ios-push-certificate) 

5. [Hoja de procuraciones](https://docs.google.com/spreadsheets/d/1wIS9zIhIgyyN8IgauaEla7bcCynk2erwt8YsiHqJUCc/edit#gid=0)  y [Krux IDs](https://docs.google.com/spreadsheets/d/1Cc9MmuvRC3u0AyY0O8Qn1BI6tteT-aw75dXhCBqZ6H4/edit#gid=1320889382)

### Personalizando la App

##### Ocultar o Mostrar Mapa Geolocalizable para pruebas

comentar la linea adecuada en el codigo te hara mostrar u ocultar el mapa geolocalizable para realizar pruebas antes del festival, puedes modificar esto en la clase `MapViewController.m`

```objective-c

- (void)refreshMap {
   NSString *mapType = [[[SPEvent currentEvent] fullEventDict][@"event"][@"mapType"] lowercaseString];
   if ([mapType isEqualToString:@"geojson"]) {
      if ([self connected]) {
         [self.mapSwitchVC switchToGeoMap];
      } else {
         [self.mapSwitchVC switchToImageMap];
      }
   } else {
     // [self.mapSwitchVC switchToGeoMap];
     [self.mapSwitchVC switchToImageMap];
   }
}
```

##### Activar Apple Music 

- Primeramente necesitas crear un certificado ([MusicKit identifier](https://help.apple.com/developer-account/#/devce5522674))  y generar una llave privada (archivo .p8).

- Necesitas crear un JWT token en donde necesitaras la llave privada, el identificador de la llave, ademas de la duración en horas y el identificador del equipo aqui una utilidad para generarlo [Apple-music-token-generator](https://github.com/pelauimagineering/apple-music-token-generator).

- Con el token generado necesitas copiarlo en  `AppleMusicManager.m` en:
```objective-c

   -(NSString *)fetchDeveloperToken {
      NSString *developerAuthenticationToken =
      @"eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjQyMzY3SjI4OUwifQ."
     @"eyJpc3MiOiIyU1Q1SkEyWkI4IiwiaWF0IjoxNTI1NDU3NTkzLCJleHAiOjE1MjU1MDA3OTN9.7vYdLMi-"
     @"K1HMpBYvw39YrKpLFJ5Cq4tYZtxaQRIOD1SnDv1zTlR8mmx2XzTLMy9gEtkJnxLtNhWK9dGhfVXqAw";
     return developerAuthenticationToken;
    }
```

- Tambien necesitaras conocer el identificador de la playlist que vas mostrar y lo puedes modificar en `Constants.h`

```objective-c
  #define idplaylist @"pl.6f1dc1b93603418f820d7d0308563375" // id playlist
```
- Una vez reunidos estos requisitos se procede a descomentar las siguientes funciones y delegados en algunas clases:

- En `MenuInfoVC.h` y  `MenuInfoVC.m`:

```objective-c
//#import "AppleMusicController.h"

@interface MenuInfoVC : SkinnedBaseViewController

//@property (nonatomic, strong) AppleMusicController *applemusicController;
- (void)updateTabBarItem;
@end
```
```objective-c
if (@available(iOS 10.3, *)) {
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:info[@"link"]]];
  // In case of implement Apple Music 
  //self.applemusicController = [[AppleMusicController alloc] init];
  // [[NSNotificationCenter defaultCenter] postNotificationName:@"pushApplePlayList" object:self.applemusicController];
}
```
- En `MenuTabBarController.h` y  `MenuTabBarController.m` :
```objective-c
#import <UIKit/UIKit.h>

//#import "AppleMusicController.h"
@interface MenuTabBarController : UITabBarController
//@property (nonatomic, strong) AppleMusicController *applemusicController;

@end
```

```objective-c
//#import "MusicViewController.h"


//@interface MenuTabBarController () <SlideNavigationControllerDelegate, UITabBarControllerDelegate,
//SKCloudServiceSetupViewControllerDelegate>


```
```objective-c
//[[NSNotificationCenter defaultCenter] addObserver:self
//selector:@selector(handleAuthorizationManagerDidUpdateNotification) name:cloudServiceDidUpdateNotification
//object:nil]

//[[NSNotificationCenter defaultCenter] removeObserver:self
//                                                name:cloudServiceDidUpdateNotification
//                                              object:nil];
```


```objective-c
-(void)cloudServiceSetupViewControllerDidDismiss:(SKCloudServiceSetupViewController *)cloudServiceSetupViewController

- (void)presentPlaylistVC 

- (void)presentCloudServiceSetup

- (void)handleAuthorizationManagerDidUpdateNotification

- (void)pushApplePlayList:(NSNotification *)notification
```

- Para mas referencias con respecto a la API [Apple Music API Reference](https://developer.apple.com/documentation/applemusicapi#//apple_ref/doc/uid/TP40017625-CH2-SW1) 

##### Mostrar u Ocultar Banners 

Usualmente se necesita mostrar algun banner con algún vinculo personalizado en alguna o todas  las vistas principales ya sea en la parte superior o inferior de la vista. Por lo que puedes utilizar la funciones llamadas `hidemainbanner` , `hidemarketbanner` , `hidebanner` y `bannerConfiguration`  para ocultar o mostar el banner y `addgesturetobanner`  para colocar el gesto hacia algun vinculo personalizado en las clases de las vistas principales. 

- `LinepVC.m`
- `MapViewController.m`
- `MenuInfoVC.m`
- `PerfilViewController.m`

y se puede modificar el vinculo en `bannerTaped` :

```objective-c
  [[UIApplication sharedApplication]
  openURL:[NSURL URLWithString:@"https://open.spotify.com/track/1tg4Sm0RY0HMReaI0YmJuM"]];
```
La imagen del banner se debe agregar a los assets del festival `Festival.xcassets`

##### Ocultar o Mostrar Grid en UISegmentedControll

En algunos eventos no se utiliza el grid o aparece con forme existan actualizaciones en la app, o tambien podria ser necesario colocar los nombres en ingles o español, por lo que puedes comentar o modificar la linea que convenga en la clase `Lineup.m`

```objective-c

self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[ @"LINEUP", @"HORARIOS", @"GRID" ]];

// self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[ @"LINEUP", @"HORARIOS"]];
```

##### Lanzar notificaciones locales cuando existan horarios

Con esta función puedes lanzar las notificaciones locales subiendo un nuevo build a tienda mediante el numero de versión, donde puedes modificarlo de acuerdo a la disponibilidad de horarios en la clase `MenuTabBarController.m`.

```objective-c

- (void)recoverLocalNotificationWithVersion{
  NSString *currentAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];

   if ([currentAppVersion isEqualToString:@"10"]) {
     NSLog(@"i am recovering local nots");
     SPUser *currentUser = [SPUser defaultUser];

     if (currentUser) {
       [currentUser forceReloadSessionFile];
       [currentUser recoverlocalNotifications];
     }
   }
}
```


### Dependencias

##### 3rd party

- [AFNetworking](https://github.com/AFNetworking/AFNetworking)
- [ZDCChat](https://github.com/zendesk/zendesk_sdk_chat_ios)
- [MRProgress](https://github.com/mrackwitz/MRProgress)
- [SDWebImage](https://github.com/SDWebImage/SDWebImage)
- [Button](https://github.com/button/button-ios)
- [MarqueeLabel](https://github.com/cbpowell/MarqueeLabel)
- [iOS-Slide-Menu](https://github.com/aryaxt/iOS-Slide-Menu)
- [OneSignal](https://github.com/OneSignal/OneSignal-iOS-SDK)
- [DateTools](https://github.com/MatthewYork/DateTools)

### License

```
Copyright © 2018 Soft Poppler S.A. de C.V. All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
