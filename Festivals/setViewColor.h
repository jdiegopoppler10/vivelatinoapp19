//
//  setViewColor.h
//  Festivals
//
//  Created by COCOLABPOPPLER on 13/06/18.
//  Copyright © 2018 Jorge Diego. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface setViewColor : NSObject

#pragma mark - Lineup
//L-Lineup
+ (UIColor *)LNowButtonColor;

#pragma mark - LineupCollection Colors
//LC-LineupCollection

+ (UIColor *)LCArtistLabelColor;
+ (UIColor *)LCLineViewTintColor;
+ (UIColor *)LCMainViewTintColor;


#pragma mark - CompactSchedule and Day Selector
//DS-DaySelector

+ (UIColor *)DStextColor;
+ (UIColor *)DShighlightColor;
+ (UIColor *)DSTintColor;

//CS-CompactSchedule

+ (UIColor *)CSGenFavBtnColor;
+ (UIColor *)CSFavBtnColor;

+ (UIColor *)CSArtistlabelColor;
+ (UIColor *)CSSchedulelabelColor;
+ (UIColor *)CSStagelabelColor;
+ (UIColor *)CSTimelabelColor;
+ (UIColor *)CSCellTintColor;

+ (UIColor *)CSHeaderTitleLabelColor;
+ (UIColor *)CSHeaderViewColor;

//WHN Colors (Whats happening Now)

+ (UIColor *)WHNTitleTextColor;
+ (UIColor *)WHNScheduleTextColor;
+ (UIColor *)WHNStageTextColor;


#pragma mark - DetailViewColors
//AD-ArtistDetail, FD-FestivalDetail, CD-CommodityDetail

+ (UIColor *)ADBorderColor;
+ (UIColor *)ADIconColor;
+ (UIColor *)ADGenreLabelColor;
+ (UIColor *)ADSesionLabelColor;
+ (UIColor *)ADCityLabelColor;
+ (UIColor *)ADBioLabelColor;

+ (UIColor *)FDBorderColor;
+ (UIColor *)FDIconColor;
+ (UIColor *)FDGenreLabelColor;
+ (UIColor *)FDSesionLabelColor;
+ (UIColor *)FDCityLabelColor;
+ (UIColor *)FDBioLabelColor;

+ (UIColor *)CDBorderColor;
+ (UIColor *)CDIconColor;
+ (UIColor *)CDGenreLabelColor;
+ (UIColor *)CDSesionLabelColor;
+ (UIColor *)CDCityLabelColor;
+ (UIColor *)CDBioLabelColor;


#pragma mark - MenuInfo Colors
//M-Menu,I-Info

+ (UIColor *)MITitleLabelColor;
+ (UIColor *)MIIconViewColor;
+ (UIColor *)MICellTintFirstColor;
+ (UIColor *)MICellTintSecondColor;

#pragma mark - News Colors
//N-News
+ (UIColor *)NLabelColor;
+ (UIColor *)NBorderColor;
+ (UIColor *)NBriefLabelColor;


#pragma mark - Profile Colors

//LP-Login Prompt

+ (UIColor *)LPLabeltextColor;
+ (UIColor *)LPButtonTintColor;
+ (UIColor *)LPButtonTextColor;


//RVC-RegistrationVC

+ (UIColor *)RVCButtonTintColor;
+ (UIColor *)RVCButtonTextColor;
+ (UIColor *)RVCMailButtonTextColor;
+ (UIColor *)RVCFBButtonTextColor;

//PI-Profile Image

+ (UIColor *)PIBorderColor;

//P-Profile (Favorite section)

+ (UIColor *)PButtonTintColor;
+ (UIColor *)PTitleLabelTextColor;
+ (UIColor *)PScheduleLabelTextColor;
+ (UIColor *)PMainLabelTextColor;

+ (UIColor *)PHeaderLabelTextColor;
+ (UIColor *)PBorderViewColor;

#pragma mark - Maps Colors
//M-Maps
+ (UIColor *)MTintColor;
+ (UIColor *)MLabelColor;
+ (UIColor *)MBorderColor;
+ (UIColor *)MOpenButtonColor;


#pragma mark - Faqs Colors
//F-Faqs

+ (UIColor *)FAnswerLabelTextColor;
+ (UIColor *)FQuestionLabelTextColor;
+ (UIColor *)FSeparatorViewColor;

@end
