//
//  setViewColor.m
//  Festivals
//
//  Created by COCOLABPOPPLER on 13/06/18.
//  Copyright © 2018 Jorge Diego. All rights reserved.
//

#import "setViewColor.h"
#import "SPEvent.h"

@implementation setViewColor



//Lineup
+ (UIColor *)LNowButtonColor{
    return [[SPEvent currentEvent] highlightColor];
}

//LineupCollection

+ (UIColor *)LCArtistLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)LCLineViewTintColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)LCMainViewTintColor{
    return [[SPEvent currentEvent] topColor];
}

//CompactSchedule and Grid

+ (UIColor *)DStextColor{
    return [[SPEvent currentEvent] iconColor];
}

+ (UIColor *)DShighlightColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)DSTintColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

//CompactSchedule


+ (UIColor *)CSGenFavBtnColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)CSFavBtnColor{
    return [[SPEvent currentEvent] topColor];
   
}


+ (UIColor *)CSArtistlabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)CSSchedulelabelColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)CSStagelabelColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)CSTimelabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)CSCellTintColor{
    return [UIColor whiteColor];
}

+ (UIColor *)CSHeaderTitleLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)CSHeaderViewColor{
    return [[SPEvent currentEvent] topColor];
}

//WHNColors

+ (UIColor *)WHNTitleTextColor{
    return [[SPEvent currentEvent] overColor];
}

+ (UIColor *)WHNScheduleTextColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)WHNStageTextColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

//ArtistDetail, FestivalDetail, CommodityDetail

+ (UIColor *)ADBorderColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)ADIconColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)ADGenreLabelColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)ADSesionLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)ADCityLabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)ADBioLabelColor{
    return [UIColor blackColor];
}


+ (UIColor *)FDBorderColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)FDIconColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)FDGenreLabelColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)FDSesionLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)FDCityLabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)FDBioLabelColor{
    return [UIColor blackColor];
}


+ (UIColor *)CDBorderColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)CDIconColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)CDGenreLabelColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)CDSesionLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)CDCityLabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)CDBioLabelColor{
    return [UIColor blackColor];
}



//MenuInfo

+ (UIColor *)MITitleLabelColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)MIIconViewColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)MICellTintFirstColor{
    return [[SPEvent currentEvent] iconColor];
}

+ (UIColor *)MICellTintSecondColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

//News

+ (UIColor *)NLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)NBriefLabelColor{
    return [UIColor blackColor];
}

+ (UIColor *)NBorderColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}


//Login Prompt

+ (UIColor *)LPLabeltextColor{
    return [UIColor whiteColor];
}

+ (UIColor *)LPButtonTintColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)LPButtonTextColor{
    return [[SPEvent currentEvent] highlightColor];
}

//RegistrationVC

+ (UIColor *)RVCButtonTintColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)RVCButtonTextColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)RVCMailButtonTextColor{
    return [[SPEvent currentEvent] overColor];
}

+ (UIColor *)RVCFBButtonTextColor{
    return [[SPEvent currentEvent] overColor];
}

//Profile (Favorite section)

+ (UIColor *)PButtonTintColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)PTitleLabelTextColor{
    return [UIColor blackColor];
}

+ (UIColor *)PMainLabelTextColor{
    return [UIColor blackColor];
}

+ (UIColor *)PHeaderLabelTextColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)PBorderViewColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)PScheduleLabelTextColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

//Profile image seccion

+ (UIColor *)PIBorderColor{
    return [[SPEvent currentEvent] overColor];
}

//Maps


+ (UIColor *)MTintColor{
    return [UIColor whiteColor];
}

+ (UIColor *)MLabelColor{
    return [[SPEvent currentEvent] highlightColor];
}

+ (UIColor *)MBorderColor{
    return [[SPEvent currentEvent] lineupNameBackground];
}

+ (UIColor *)MOpenButtonColor{
    return [[SPEvent currentEvent] overColor];
}


//Faqs

+ (UIColor *)FAnswerLabelTextColor{
    return [UIColor blackColor];
}

+ (UIColor *)FQuestionLabelTextColor{
    return [[SPEvent currentEvent] topColor];
}

+ (UIColor *)FSeparatorViewColor{
    return [[SPEvent currentEvent] overColor];
}



@end
